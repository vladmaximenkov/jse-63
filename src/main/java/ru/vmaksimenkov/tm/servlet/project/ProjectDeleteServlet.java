package ru.vmaksimenkov.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/delete/*")
public class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp) throws ServletException, IOException {
        ProjectRepository.getInstance().removeById(req.getParameter("id"));
        resp.sendRedirect("/projects");
    }

}
