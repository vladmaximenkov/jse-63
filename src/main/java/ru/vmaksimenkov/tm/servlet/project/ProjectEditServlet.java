package ru.vmaksimenkov.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        @Nullable final Project project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        if (id == null) return;
        @Nullable final String name = req.getParameter("name");
        @Nullable final String description = req.getParameter("description");
        @Nullable final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        ProjectRepository.getInstance().add(project);
        resp.sendRedirect("/projects");
    }

}
