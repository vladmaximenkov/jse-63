package ru.vmaksimenkov.tm.servlet.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        @Nullable final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final String id = req.getParameter("id");
        if (id == null) return;
        @Nullable final String name = req.getParameter("name");
        @Nullable final String description = req.getParameter("description");
        @NotNull final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        TaskRepository.getInstance().add(task);
        resp.sendRedirect("/tasks");
    }

}
