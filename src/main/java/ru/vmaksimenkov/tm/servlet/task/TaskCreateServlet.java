package ru.vmaksimenkov.tm.servlet.task;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/create/*")
public class TaskCreateServlet extends HttpServlet {

    @Override
    protected void doGet(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp) throws ServletException, IOException {
        TaskRepository.getInstance().create();
        resp.sendRedirect("/tasks");
    }

}
