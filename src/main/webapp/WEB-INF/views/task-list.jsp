<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Task list</h1>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

<table>
    <tr>
        <th style="width:350px">id</th>
        <th style="width:300px">name</th>
        <th>desc</th>
        <th style="width:75px">edit</th>
        <th style="width:75px">delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td><c:out value="${task.id}"/></td>
            <td><c:out value="${task.name}"/></td>
            <td><c:out value="${task.description}"/></td>
            <td><a href="/task/edit/?id=${task.id}">edit</a></td>
            <td><a href="/task/delete/?id=${task.id}">delete</a></td>
        </tr>
    </c:forEach>
</table>


<form action="/task/create">
    <button>Create task</button>
</form>

<jsp:include page="../include/_footer.jsp"/>