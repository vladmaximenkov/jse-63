<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Edit project</h1>

<form action="/project/edit?id=${project.id}" method="post">
    <p>
        Name:
        <input type="text" name="name" value="${project.name}"/></p>
    <p>
        Description:
        <input type="text" name="description" value="${project.description}"/>
    </p>

    <input type="hidden" name="id" value="${project.id}"/>
    <button type="submit">Save</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
